# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as cig with context %}

{% if salt['pillar.get']('cig-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_cig', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

cig-package-install-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

cig-package-install-bin-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/bin
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - cig-package-install-user-{{ name }}-present

cig-package-install-cig-bin-{{ name }}-installed:
  file.managed:
    - name: {{ home }}/bin/cig
    - source: {{ cig.source }}
    - source_hash: {{ cig.source_hash }}
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0700'
    - require:
      - cig-package-install-bin-dir-{{ name }}-managed
{% endif %}
{% endfor %}
{% endif %}
