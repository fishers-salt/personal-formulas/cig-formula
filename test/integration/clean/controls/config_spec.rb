# frozen_string_literal: true

control 'cig-config-file-config-auser-absent' do
  title 'should be absent'

  describe file('/home/auser/.cig.yaml') do
    it { should_not exist }
  end
end
