# frozen_string_literal: true

control 'cig-package-install-cig-bin-auser-installed' do
  title 'should not exist'

  describe file('/home/auser/bin/cig') do
    it { should_not exist }
  end
end
