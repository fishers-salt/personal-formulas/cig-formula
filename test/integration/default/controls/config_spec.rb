# frozen_string_literal: true

control 'cig-config-file-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.cig.yaml') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('code: /home/auser/code') }
    its('content') { should include('tmp: /home/auser/tmp') }
  end
end
