# frozen_string_literal: true

control 'cig-package-install-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'cig-package-install-bin-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/bin') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'cig-package-install-cig-bin-auser-installed' do
  title 'should exist'

  describe file('/home/auser/bin/cig') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0700' }
  end
end
